#! /usr/bin/python3

import os
import selenium
from selenium import webdriver
import time
import io
import requests
from selenium.common.exceptions import ElementClickInterceptedException

import smtplib
from email.message import EmailMessage
from os.path import basename
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import time



to = 'annshashini92@gmail.com'

import email_ahe as ahe


def scroll_to_end(driver):
    
    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    time.sleep(.1)

def get_links(driver):
    print("Next page") 
    ul_data = driver.find_elements_by_tag_name('ul')
    ul2_li =  ul_data[0].find_elements_by_tag_name('li') 

    #urls = []
    for n in ul2_li:
        atag = n.find_elements_by_tag_name('a')
        try:
            url = atag[0].get_attribute('href')
            #urls.append(url)
            write_url_file(url)
            #print("{};{}".format(n.text,url))
        except:
            pass
            #print(n.text)

    nxt_link = driver.find_element_by_css_selector('div.mw-allpages-nav')
    #print(nxt_link.text)
    a = nxt_link.find_elements_by_tag_name('a')
    #print(len(a))
    if (len(a)==1):
        print("End of pages")
        exit()
    if(len(a)==2):
        url2 = a[1].get_attribute('href')
        driver.get(url2)
        time.sleep(5)
        scroll_to_end(driver)
     
        get_links(driver)


    #fn_time = '../data/derana_hot_news_{}.txt'.format(time.strftime("%d%m%y_%H%M"))
    #for i,u in enumerate(urls):
        
    #    postdata = get_posttext(u,driver)
    #    print("{}/{}".format(i,len(urls)))
    #    with open(fn_time,'a') as fp:
    #        for d in  postdata:
    #            fp.write('{}\n'.format(d))
    
    #ahe.nortify_me(to,fn_time)

     
def write_url_file(data,file_name = 'url_ahe_wiki.csv'):
    '''Write file in append mode'''
    with open(file_name,'a+') as fp:
        fp.write('{}\n'.format(data));

def get_posttext(url,driver):
    driver.get(url)
    time.sleep(1)
    scroll_to_end(driver)
    data = [];
    p_tags = driver.find_elements_by_tag_name('p')
    try:
        #skipping the copyright tag which is in english
        for p in p_tags[0:-2]:
            data.append(p.text)
        #print(data)
    except:
        print('Error in getting p data')
    return data





if __name__=='__main__':
    #os.environ['MOZ_HEADLESS'] = '1' 
    
    driver = webdriver.Firefox()
    
    url = 'https://si.wikipedia.org/wiki/%E0%B7%80%E0%B7%92%E0%B7%81%E0%B7%9A%E0%B7%82:%E0%B7%83%E0%B7%92%E0%B6%BA%E0%B7%85%E0%B7%94_%E0%B6%B4%E0%B7%92%E0%B6%A7%E0%B7%94/%E0%B6%85%E0%B6%85'
    driver.get(url)
    time.sleep(5)
    scroll_to_end(driver)
    
    get_links(driver)
