#! /usr/bin/python3

import os
import selenium
from selenium import webdriver
import time
import io
import requests
from selenium.common.exceptions import ElementClickInterceptedException

import smtplib
from email.message import EmailMessage
from os.path import basename
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import time


to = 'annshashini92@gmail.com'
file_save = '/home/hasith/Documents/Projects/Shared/NLP_ahe/data'
file_last_url = '/home/hasith/Documents/Projects/Shared/NLP_ahe/data/.derana_status'

import email_ahe as ahe


def scroll_to_end(driver):
    
    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    time.sleep(.1)

def get_story(driver,oldurl):
    news = driver.find_elements_by_tag_name('h2')
    urls = []
    for n in news:
        atag = n.find_elements_by_tag_name('a')
        urls.append(atag[0].get_attribute('href'))
        #print("{}".format(n.text))
    print('got {} links'.format(len(urls)))
    fn_time = '{}/derana_hot_news_{}.txt'.format(file_save,time.strftime("%d%m%y_%H%M"))
    
    for i,u in enumerate(urls):

        if (u==oldurl):
            print('Reached old url')
            break

        postdata = get_posttext(u,driver)
        print("{}/{}".format(i,len(urls)))
        with open(fn_time,'a+') as fp:
            for d in  postdata:
                fp.write('{}\n'.format(d))
    
    ahe.nortify_me(to,fn_time)
    ahe.nortify_me('hasith@fos.cmb.ac.lk',fn_time)
    write_urlfile(urls[0])
    
def write_url_file(data,file_name = 'url_ahe.csv'):
    '''Write file in append mode'''
    with open(file_name,'a') as fp:
        fp.write('{}\n'.format(data));

def get_posttext(url,driver):
    driver.get(url)
    time.sleep(1)
    scroll_to_end(driver)
    data = [];
    p_tags = driver.find_elements_by_tag_name('p')
    try:
        #skipping the copyright tag which is in english
        for p in p_tags[0:-2]:
            data.append(p.text)
        #print(data)
    except:
        print('Error in getting p data')
    return data


def load_urlfile():
    try:
        with open(file_last_url,'r') as fp:
            old_url = fp.read()
        return old_url
    except:
        print("file not found")
        return []

def write_urlfile(url):

    with open(file_last_url,'w') as fp:
        fp.write(url)

if __name__=='__main__':
    print("[{}]\tStart".format(time.strftime('%H:%M:%s')))

    os.environ['MOZ_HEADLESS'] = '1' 
    driver = webdriver.Firefox()
    
    url = 'http://sinhala.adaderana.lk/sinhala-hot-news.php'
    old_url = load_urlfile()

    driver.get(url)
    print('got page')
    time.sleep(5)
    scroll_to_end(driver)
    print('sel working')
    get_story(driver,old_url)
    print("[{}]\tdone".format(time.strftime('%H:%M:%s')))
