#! /usr/bin/python3

import os
import selenium
from selenium import webdriver
import time
import io
import requests
from selenium.common.exceptions import ElementClickInterceptedException

import smtplib
from email.message import EmailMessage
from os.path import basename
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import time

import argparse

to = 'annshashini92@gmail.com'

import email_ahe as ahe


def scroll_to_end(driver):
    
    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    time.sleep(.1)

def get_links(driver):
    print("Next page") 
    ul_data = driver.find_elements_by_tag_name('ul')
    ul2_li =  ul_data[0].find_elements_by_tag_name('li') 

    #urls = []
    for n in ul2_li:
        atag = n.find_elements_by_tag_name('a')
        try:
            url = atag[0].get_attribute('href')
            #urls.append(url)
            write_url_file(url)
            #print("{};{}".format(n.text,url))
        except:
            pass
            #print(n.text)

    nxt_link = driver.find_element_by_css_selector('div.mw-allpages-nav')
    #print(nxt_link.text)
    a = nxt_link.find_elements_by_tag_name('a')
    #print(len(a))
    if (len(a)==1):
        print("End of pages")
        exit()
    if(len(a)==2):
        url2 = a[1].get_attribute('href')
        driver.get(url2)
        time.sleep(5)
        scroll_to_end(driver)
     
        get_links(driver)


    #fn_time = '../data/derana_hot_news_{}.txt'.format(time.strftime("%d%m%y_%H%M"))
    #for i,u in enumerate(urls):
        
    #    postdata = get_posttext(u,driver)
    #    print("{}/{}".format(i,len(urls)))
    #    with open(fn_time,'a') as fp:
    #        for d in  postdata:
    #            fp.write('{}\n'.format(d))
    
    #ahe.nortify_me(to,fn_time)

     
def write_url_file(data,file_name = 'url_ahe_wiki.csv'):
    '''Write file in append mode'''
    with open(file_name,'a+') as fp:
        fp.write('{}\n'.format(data));

def get_wikipage(url,driver):
    driver.get(url)
    time.sleep(1)
    scroll_to_end(driver)
    data = [];
    body = driver.find_element_by_css_selector('div.mw-body')
    for para in body.find_elements_by_tag_name('p'):
        dt = para.text

        if (len(dt)>0):
            write_url_file(dt,'sinhala_wiki_content.txt')
        write_url_file('','sinhala_wiki_content.txt') 

def last_page(i):
    with open('../data/.wiki_ahe','w') as fp:
        fp.write('{}'.format(i))


if __name__=='__main__':
    #os.environ['MOZ_HEADLESS'] = '1' 
    
    driver = webdriver.Firefox()
    
    fn = '../data/url_ahe_wiki.csv'
    parser = argparse.ArgumentParser()
    parser.add_argument('-s','--start')
    parser.add_argument('-n','--no')

    args = parser.parse_args()


    #print(args.start)
    #print(args.no)
     
    #start = int(args.start)
    n = int(args.no)+1
    
    with open('../data/.wiki_ahe','r') as fp:
        lasturl = fp.read()
    
    start = int(lasturl)+1

    with open(fn,'r') as fp:
        uall = fp.read()
    
    urls = uall.split('\n')[start:start+n]
    
    for i,url in enumerate(urls):
        print("{}/{}".format(start+i,start+n))
        driver.get(url)
        get_wikipage(url,driver)
        last_page(start+i)

